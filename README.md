# Board  
[GitHub](https://github.com/mcauser/MCUDEV_DEVEBOX_H7XX_M)  
# Connection  
Leds connection common cathode  
K1 [Button] - PE3  
K2 [Button] - PC5  
LD2 [LED] - PA1  
  
KEY_EX1 [Button] - PB9  
LD3 - PA2  
LD4 - PA3  
LD5 - PA4  
LD6 - PA5  
